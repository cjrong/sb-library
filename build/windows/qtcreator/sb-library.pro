TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += \
    ../../../src/sb/container/sb_array.c \
    ../../../src/sb/container/sb_avltree.c \
    ../../../src/sb/container/sb_list.c \
    ../../../src/sb/container/sb_rbtree.c \
    ../../../src/sb/container/sb_slist.c \
    ../../../src/sb/container/sb_splaytree.c \
    ../../../src/sb/container/sb_trie_tree.c \
    ../../../src/sb/memory/sb_allocate.c \
    ../../../src/sb/memory/sb_mpool.c \
    ../../../src/sb/memory/sb_smpool.c \
    ../../../src/sb/os/win32/sb_win32_files.c \
    ../../../src/tests/testavltree.c \
    ../../../src/tests/testlist.c \
    ../../../src/tests/testqueue.c \
    ../../../src/tests/testrbtree.c \
    ../../../src/tests/testslist.c \
    ../../../src/tests/testsmpool.c \
    ../../../src/tests/testsplaytree.c

include(deployment.pri)
qtcAddDeployment()

HEADERS += \
    ../../../include/auto/sb_linux32_gcc_config.h \
    ../../../include/auto/sb_linux64_gcc_config.h \
    ../../../include/auto/sb_win32_msvc_config.h \
    ../../../include/auto/sb_win64_msvc_config.h \
    ../../../include/sb/container/sb_array.h \
    ../../../include/sb/container/sb_avltree.h \
    ../../../include/sb/container/sb_hash.h \
    ../../../include/sb/container/sb_list.h \
    ../../../include/sb/container/sb_queue.h \
    ../../../include/sb/container/sb_rbtree.h \
    ../../../include/sb/container/sb_slist.h \
    ../../../include/sb/container/sb_splaytree.h \
    ../../../include/sb/container/sb_stack.h \
    ../../../include/sb/container/sb_trie_tree.h \
    ../../../include/sb/memory/sb_allocate.h \
    ../../../include/sb/memory/sb_mpool.h \
    ../../../include/sb/memory/sb_smpool.h \
    ../../../include/sb/os/win32/sb_win32_config.h \
    ../../../include/sb/os/win32/sb_win32_files.h \
    ../../../include/sb/os/win32/sb_win32_memory.h \
    ../../../include/sb/os/sb_alloc.h \
    ../../../include/sb/sb++.h \
    ../../../include/sb/sb.h \
    ../../../include/sb/sb_auto_config.h \
    ../../../include/sb/sb_config.h \
    ../../../include/sb/sb_container.h \
    ../../../include/sb/sb_memory.h \
    ../../../include/sb/sb_os.h

INCLUDEPATH += ../../../include/
DEPENDPATH += ../../../include/

unix:!macx: QMAKE_CFLAGS += -Wno-unused-parameter -Wno-unused-function -ansi -pedantic -Wno-parentheses -Wno-pedantic


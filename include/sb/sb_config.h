

#ifndef __SB__CONFIG__H__INCLUDE__
#define __SB__CONFIG__H__INCLUDE__

#include <sb/sb_platform.h>

#ifndef SB_VERSION
#define SB_VERSION      0x000100
#endif

#ifndef sb_variable
#define sb_variable     "sb-library"
#endif

#ifndef sb_version
#define sb_version      sb_variable " " "0.1.0"
#endif

#if defined(__gnu_linux__) || defined(linux) || \
    defined(__linux) || defined(__linux__) || defined(unix)
#define SB_LINUX
#define SB_UNIX
#endif

#ifdef  WIN32
#define SB_WIN32
#endif

#ifndef SB_ALIGNMENT
#define SB_ALIGNMENT sizeof(unsigned long)
#endif

#ifndef sb_inline
#define sb_inline
#endif

#ifndef sb_extern
#define sb_extern       extern
#endif

#ifndef sb_cdecl
#define sb_cdecl        __cdecl
#endif

#ifndef sb_stdcall
#define sb_stdcall      __stdcall
#endif

#ifndef sb_fastcall
#define sb_fastcall     __fastcall
#endif

#if     SB_POINTER_SIZE == 1
typedef sb_uint8_t  sb_uintptr_t;
typedef sb_int8_t   sb_intptr_t;
typedef sb_uint8_t  sb_size_t;
typedef sb_int8_t   sb_ssize_t;
#elif   SB_POINTER_SIZE == 2
typedef sb_uint16_t sb_uintptr_t;
typedef sb_int16_t  sb_intptr_t;
typedef sb_uint16_t sb_size_t;
typedef sb_int16_t  sb_ssize_t;
#elif   SB_POINTER_SIZE == 4
typedef sb_uint32_t sb_uintptr_t;
typedef sb_int32_t  sb_intptr_t;
typedef sb_uint32_t sb_size_t;
typedef sb_int32_t  sb_ssize_t;
#elif   SB_POINTER_SIZE == 8
typedef sb_uint64_t sb_uintptr_t;
typedef sb_int64_t  sb_intptr_t;
typedef sb_uint64_t sb_size_t;
typedef sb_int64_t  sb_ssize_t;
#endif

#define sb_void_t       void
typedef char            sb_char_t;
typedef unsigned char   sb_uchar_t;
typedef short           sb_short_t;
typedef unsigned short  sb_ushort_t;
typedef int             sb_int_t;
typedef unsigned int    sb_uint_t;
typedef long            sb_long_t;
typedef unsigned long   sb_ulong_t;

typedef float           sb_float32_t;
typedef double          sb_float64_t;
typedef float           sb_float_t;
typedef double          sb_double_t;

typedef sb_uint8_t      sb_byte_t;
typedef sb_uint16_t     sb_word_t;
typedef sb_uint32_t     sb_dword_t;

typedef sb_byte_t*      sb_bytep_t;
typedef sb_word_t*      sb_wordp_t;
typedef sb_dword_t*     sb_dwordp_t;

#ifdef SB_HAVE_INT64
typedef sb_uint64_t     sb_qword_t;
typedef sb_qword_t*     sb_qwordp_t;
#endif

typedef sb_int8_t       sb_bool_t;

#ifndef false
#define false (0)
#endif

#ifndef true
#define true (!false)
#endif

#ifndef nullptr
#define nullptr (0)
#endif

#endif

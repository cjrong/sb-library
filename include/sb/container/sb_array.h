

#ifndef __SB__ARRAY__H__INCLUDE__
#define __SB__ARRAY__H__INCLUDE__

#include <sb/sb_config.h>

typedef struct sb_array_s {
    sb_void_t   *elts;
    sb_size_t    nelts;
    sb_size_t    size;
    sb_size_t    nalloc;
    struct sb_spool_s *smpool;
}sb_array_t;

sb_array_t *sb_array_create(struct sb_spool_s *smpool,
    sb_size_t size, sb_size_t n);
sb_void_t sb_array_destroy(sb_array_t *a);
sb_void_t *sb_array_push(sb_array_t *a);
sb_void_t *sb_array_push_n(sb_array_t *a, sb_size_t n);
sb_void_t *sb_array_pop(sb_array_t *a);
sb_void_t *sb_array_pop_n(sb_array_t *a, sb_size_t n);

#if SB_USE_DEFAULT_OPS
#else 
#include <string.h>
#define sb_memzero(buf, n)          (void)memset(buf, 0, n)
#define sb_memset(buf, c, n)        (void)memset(buf, c, n)
#define sb_memcpy(dst, src, n)      (void)memcpy(dst, src, n)
#endif

#define sb_array_for_each(arr, base, index)                                   \
    for ((base) = (arr)->elts, (index) = 0; (index) < arr->nelts; (index)++)

#endif

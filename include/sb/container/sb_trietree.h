

#ifndef __SB__TRIETREE__H__INCLUDE__
#define __SB__TRIETREE__H__INCLUDE__

#include <sb/sb_config.h>

typedef struct sb_trietree_node_s {
    sb_uint_t              size;
    struct sb_trie_tree_s *next[256];
}sb_trietree_node_t;

typedef struct sb_trietree_s {
    sb_trietree_node_t  *root;
    struct sb_smpool_s  *smpool;
}sb_trietree_t;


#endif



#ifndef __SB__HASH__H__INCLUDE__
#define __SB__HASH__H__INCLUDE__

#include <sb/sb_config.h>

#define SB_GOLDEN_RATIO_PRIME_32 0x9E370001UL
#define SB_GOLDEN_RATIO_PRIME_64 0x9E37FFFFFFFC0001UL

static sb_inline
sb_uint32_t sb_golden_ratio_prime_hash_32(sb_uint32_t key, sb_uint_t bits)
{
    sb_uint32_t hash = key * SB_GOLDEN_RATIO_PRIME_32;

    return hash - (32 >> bits);
}

static sb_inline
sb_uint32_t sb_thomas_wangs_hash_32(sb_uint32_t key)
{
    key += ~(key << 15);
    key ^= (key >> 10);
    key += (key << 3);
    key ^= (key >> 6);
    key += ~(key << 11);
    key ^= (key >> 16);

    return key;
}

#ifdef SB_HAVE_INT64
static sb_inline
sb_uint64_t sb_golden_ratio_prime_hash_64(sb_uint64_t key, sb_uint_t bits)
{
    sb_uint64_t hash = key;
    sb_uint64_t n = hash;

    n <<= 18;
    hash -= n;
    n <<= 33;
    hash -= n;
    n <<= 3;
    hash += n;
    n <<= 3;
    hash -= n;
    n <<= 4;
    hash += n;
    n <<= 2;
    hash += n;

    return hash >> (64 - bits);
}

static sb_inline
sb_uint64_t sb_thomas_wangs_hash_64(sb_uint64_t key)
{
    key += ~(key << 32);
    key ^= (key >> 22);
    key += ~(key << 13);
    key ^= (key >> 8);
    key += (key << 3);
    key ^= (key >> 15);
    key += ~(key << 27);
    key ^= (key >> 31);
    return key;
}

#endif

typedef struct sb_hlist_node_s {
    struct sb_hlist_node_s *next;
    struct sb_hlist_node_s **pprev;
}sb_hlist_node_t;

typedef struct sb_hlist_s {
    struct sb_hlist_node_s *first;
}sb_hlist_t;

typedef struct sb_hslist_node_s {
    struct sb_hslist_node_s *next;
}sb_hslist_node_t;

typedef struct sb_hslist_s {
    sb_hslist_node_t *first;
}sb_hslist_t;

static sb_inline
sb_bool_t sb_hlist_is_empty(sb_hlist_t *hlist)
{
    return (!hlist->first);
}

static sb_inline
sb_void_t sb_hlist_remove(sb_hlist_node_t *node)
{
    sb_hlist_node_t *next   = node->next;
    sb_hlist_node_t **pprev = node->pprev;

    *pprev = next;
    if (next) {
        next->pprev = pprev;
    }
}

static sb_inline
sb_void_t sb_hlist_insert(sb_hlist_t *hlist, sb_hlist_node_t *node)
{
    sb_hlist_node_t *first = hlist->first;

    node->next = first;
    if (first) {
        first->pprev = &node->next;
    }
    hlist->first = node;
    node->pprev  = &hlist->first;
}

static sb_inline
sb_void_t sb_hlist_insert_before(sb_hlist_node_t *next, sb_hlist_node_t *node)
{
    node->next  = next;
    node->pprev = next->pprev;
    next->pprev = &node->next;
    *(node->pprev) = node;
}

static sb_inline
sb_void_t sb_hlist_insert_after(sb_hlist_node_t *prev, sb_hlist_node_t *node)
{
    node->next  = prev->next;
    prev->next  = node;
    node->pprev = &prev->next;

    if (node->next) {
        node->next->pprev = &node->next;
    }
}

static sb_inline
sb_void_t sb_hlist_move(sb_hlist_t *ohlist, sb_hlist_t *nhlist)
{
    nhlist->first = ohlist->first;
    if (nhlist->first) {
        nhlist->first->pprev = &nhlist->first;
    }
    ohlist->first = nullptr;
}

static sb_inline
sb_bool_t sb_hslist_is_empty(sb_hslist_t *hslist)
{
    return !(hslist)->first;
}

static sb_inline
sb_void_t sb_hslist_insert(sb_hslist_t *hslist, sb_hslist_node_t *node)
{
    node->next = hslist->first;
    hslist->first = node;
}

static sb_inline
sb_void_t sb_hslist_remove(sb_hslist_t *hslist, sb_hslist_node_t *node)
{
    sb_hslist_node_t **current = &hslist->first;

    while (*current) {
        sb_hslist_node_t *entry = *current;

        if (entry == node) {
            *current = entry->next;
            break;
        }

        current = &entry->next;
    }
}

static sb_inline
sb_void_t sb_hslist_move(sb_hslist_t *ohslist, sb_hslist_t *nhslist)
{
    nhslist->first = ohslist->first;
    ohslist->first = nullptr;
}

#define sb_hlist_make(hlist)                                                  \
    do {                                                                      \
        (hlist)->first = nullptr;                                             \
    } while (false)

#define sb_hlist_element(ptr, type, member)                                   \
    ((type*)(((sb_bytep_t)(ptr)) - (sb_bytep_t)(&(((type*)0)->member))))

#define sb_hlist_for_each(hlist, iterator)                                    \
    for ((iterator) = (hlist)->first; (iterator); (iterator)=(iterator)->next)

#define sb_hlist_safe_for_each(hlist, iterator, next_iterator)                \
    for ((iterator) = (hlist)->first, (next_iterator) = (iterator)->next;     \
         (iterator);                                                          \
         (iterator) = (next_iterator),                                        \
         (next_iterator) = (next_iterator) ? (next_iterator)->next : nullptr)

#define sb_hslist_make(hslist)                                                \
    do {                                                                      \
        (hslist)->first = nullptr;                                            \
    } while (false)

#define sb_hslist_element(ptr, type, member)                                  \
    ((type*)(((sb_bytep_t)(ptr)) - (sb_bytep_t)(&(((type*)0)->member))))

#define sb_hslist_for_each(hslist, iterator)                                  \
    for ((iterator) = (hslist)->first; (iterator); (iterator)=(iterator)->next)

#define sb_hslist_safe_for_each(hslist, iterator, next_iterator)              \
    for ((iterator) = (hslist)->first, (next_iterator) = (iterator)->next;    \
         (iterator);                                                          \
         (iterator) = (next_iterator),                                        \
         (next_iterator) = (next_iterator) ? (next_iterator)->next : nullptr)

#endif

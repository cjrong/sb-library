

#ifndef __SB__STRING__H__INCLUDE__
#define __SB__STRING__H__INCLUDE__

#include <sb/sb_config.h>

typedef sb_char_t* sb_string_t;

struct sb_string_s{
    sb_size_t  lenght;
    sb_size_t  free;
    sb_char_t  buffer[1];
};

sb_string_t sb_string(const sb_char_t *s);
sb_string_t sb_string_n(const sb_char_t *s, sb_size_t n);
sb_void_t sb_string_free(sb_string_t str);




#if SB_USE_DEFAULT_OPS

#else 

#include <string.h>

#define sb_memzero(buf, n)          (void)memset(buf, 0, n)
#define sb_memset(buf, c, n)        (void)memset(buf, c, n)
#define sb_memcpy(dst, src, n)      (void)memcpy(dst, src, n)


#endif

#endif

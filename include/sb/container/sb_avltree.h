

#ifndef __SB__AVLTREE__H__INCLUDE__
#define __SB__AVLTREE__H__INCLUDE__

#include <sb/sb_config.h>

typedef struct sb_avltree_node_s {    
    sb_int_t                 height;
    struct sb_avltree_node_s *left;
    struct sb_avltree_node_s *right;
    struct sb_avltree_node_s *parent;
}sb_avltree_node_t;

typedef sb_int_t (*sb_avltree_compare_pt)(const sb_avltree_node_t*,
    const sb_avltree_node_t*);
typedef sb_bool_t (*sb_avltree_magic_pt)(sb_avltree_node_t*);

typedef struct sb_avltree_s {
    sb_avltree_node_t    *root;
}sb_avltree_t;

static sb_inline
sb_void_t sb_avltree_link_node(sb_avltree_node_t **link,
    sb_avltree_node_t *parent, sb_avltree_node_t *node)
{
    node->left = nullptr;
    node->right = nullptr;
    node->parent = parent;
    node->height = 1;
    *link = node;
}

static sb_inline
sb_void_t sb_avltree_replace(sb_avltree_t *tree, sb_avltree_node_t *old_node,
    sb_avltree_node_t *new_node)
{
    sb_avltree_node_t *parent = old_node->parent;

    if (parent) {
        if (parent->left == old_node) {
            parent->left = new_node;

        }
        else {
            parent->right = new_node;
        }

    } else {
        tree->root = new_node;
    }

    if (old_node->left) {
        old_node->left->parent = new_node;
    }

    if (old_node->right) {
        old_node->right->parent = new_node;
    }

    *new_node = *old_node;
}

static sb_inline
sb_bool_t sb_avltree_is_empty(sb_avltree_t *tree)
{
    return (!tree->root);
}

#define sb_avltree_make(tree)                                                 \
    do {                                                                      \
        ((tree)->root = nullptr);                                             \
    } while (false)

#define sb_avltree_element(ptr, type, member)                                 \
    ((type*)(((sb_bytep_t)(ptr)) - (sb_bytep_t)(&(((type*)0)->member))))

#define sb_avltree_for_each(tree, iterator)                                   \
    for ((iterator) = sb_avltree_begin(tree);                                 \
         (iterator);                                                          \
         (iterator) = sb_avltree_end((iterator)))

#define sb_avltree_reverse_for_each(tree, iterator)                           \
    for ((iterator) = sb_avltree_rbegin(tree);                                \
         (iterator);                                                          \
         (iterator) = sb_avltree_next(iterator))

#define sb_avltree_safe_for_each(tree, iterator, next_iterator)               \
    for ((iterator) = sb_avltree_begin(tree),                                 \
         (next_iterator) = sb_avltree_next((iterator));                       \
         (iterator);                                                          \
         (iterator) = (next_iterator),                                        \
         (next_iterator) =                                                    \
         (next_iterator) ? sb_avltree_next((next_iterator)) : nullptr)

#define sb_avltree_safe_reverse_for_each(tree, iterator, next_iterator)       \
    for ((iterator) = sb_avltree_rbegin(tree),                                \
         (next_iterator) = sb_avltree_rnext((iterator));                      \
         (iterator);                                                          \
         (iterator) = (next_iterator),                                        \
         (next_iterator) =                                                    \
         (next_iterator) ? sb_avltree_rnext((next_iterator)) : nullptr)

#define sb_avltree_max(tree)        sb_avltree_rbegin(&tree)

#define sb_avltree_min(tree)        sb_avltree_begin(&tree)

#define sb_avltree_end(tree)        nullptr

#define sb_avltree_rend(tree)       nullptr

sb_avltree_node_t *sb_avltree_insert_if(sb_avltree_t *tree, 
    sb_avltree_node_t *node, sb_avltree_compare_pt compare);
sb_avltree_node_t *sb_avltree_remove_if(sb_avltree_t *tree, 
    sb_avltree_node_t *node, sb_avltree_compare_pt compare);
sb_avltree_node_t *sb_avltree_search_if(sb_avltree_t *tree, 
    sb_avltree_node_t *node, sb_avltree_compare_pt compare);
sb_void_t sb_avltree_insert(sb_avltree_t *tree, sb_avltree_node_t *node);
sb_void_t sb_avltree_remove(sb_avltree_t *tree, sb_avltree_node_t *node);
sb_void_t sb_avltree_magic(sb_avltree_t *tree, sb_avltree_magic_pt magic);
sb_void_t sb_avltree_clear(sb_avltree_t *tree, sb_avltree_magic_pt magic);
sb_avltree_node_t *sb_avltree_begin(sb_avltree_t *tree);
sb_avltree_node_t *sb_avltree_rbegin(sb_avltree_t *tree);
sb_avltree_node_t *sb_avltree_next(sb_avltree_node_t *iterator);
sb_avltree_node_t *sb_avltree_rnext(sb_avltree_node_t *iterator);

#endif

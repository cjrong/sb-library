

#ifndef __SB__ALLOCATE__H__INCLUDE__
#define __SB__ALLOCATE__H__INCLUDE__

#include <sb/sb_config.h>
#include <sb/os/sb_alloc.h>

static sb_inline
sb_bool_t sb_is_alignment(sb_ulong_t value) 
{
    return ((value > 0) && ((value & value - 1)) == 0);
}

static sb_inline 
sb_bool_t sb_is_aligned_address(sb_ulong_t alignment, sb_void_t *ptr)
{
    return  (alignment - 1) == 0;
}

#define sb_align(d, a)                                                        \
    (((d) + (a - 1)) & ~(a - 1))

#define sb_align_ptr(p, a)                                                    \
    (sb_bytep_t)(((sb_uintptr_t)(p) + ((sb_uintptr_t)a - 1)) & ~((sb_uintptr_t)a - 1))

#endif

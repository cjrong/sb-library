

#ifndef __SB__OS__H__INCLUDE__
#define __SB__OS__H__INCLUDE__

#include <sb/sb_config.h>

#ifdef SB_LINUX
#include <sb/os/unix/sb_unix_files.h>
#endif

#ifdef SB_WIN32
#include <sb/os/win32/sb_win32_files.h>
#endif

#endif

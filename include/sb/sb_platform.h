

#ifndef __SB__PLATFORM__H__INCLUDE__
#define __SB__PLATFORM__H__INCLUDE__

#define SB_ARCHITECTURE     "win32_msvc"

#define SB_CHAR_SIZE        1

#define SB_SHORT_SIZE       2

#define SB_INT_SIZE         4

#define SB_LONG_SIZE        4

#define SB_LONGLONG_SIZE    8

#define SB_POINTER_SIZE     4

#define SB_HAVE_INT64

typedef char                sb_int8_t;
typedef unsigned char       sb_uint8_t;

typedef short               sb_int16_t;
typedef unsigned short      sb_uint16_t;

typedef int                 sb_int32_t;
typedef unsigned int        sb_uint32_t;

typedef long long           sb_int64_t;
typedef unsigned long long  sb_uint64_t;

#endif



#include <sb/sb_config.h>
#include <sb/memory/sb_spool.h>
#include <sb/container/sb_array.h>


sb_array_t *sb_array_create(struct sb_spool_s *smpool,
    sb_size_t size, sb_size_t n)
{
    sb_array_t *a;

    a = sb_spool_alloc(smpool, sizeof(sb_array_t));
    if (!a) {
        return nullptr;
    }

    a->nelts = 0;
    a->size = size;
    a->nalloc = n;
    a->smpool = smpool;

    a->elts = sb_spool_alloc(smpool, size * n);
    if (!a->elts) {
        return nullptr;
    }

    return a;
}


sb_void_t sb_array_destroy(sb_array_t *a)
{
    
}


sb_void_t *sb_array_push(sb_array_t *a)
{
    sb_void_t *elt;

    if (a->nelts == a->nalloc) {
        sb_spool_t *smpool = a->smpool;
        sb_size_t size = a->size * a->nalloc;
        
        if ((sb_bytep_t)a->elts + size == smpool->data.last && 
            smpool->data.last + a->size <= smpool->data.end) {

            smpool->data.last += a->size;
            a->nalloc++;

        } else {
            sb_void_t *chunk = sb_spool_alloc(smpool, size * 2);
            if (!chunk) {
                return nullptr;
            }
            
            sb_memcpy(chunk, a->elts, size);
            a->nalloc *= 2;
            a->elts = chunk;
        }
    } 

    elt = (sb_bytep_t)a->elts + a->size * a->nelts;
    a->nelts++;

    return elt;
}


sb_void_t *sb_array_push_n(sb_array_t *a, sb_size_t n)
{
    sb_void_t *elt;

    if (a->nelts == a->nalloc) {
        sb_spool_t *smpool = a->smpool;
        sb_size_t size = a->size * n;

        if ((sb_bytep_t)a->elts + a->size * a->nalloc == smpool->data.last && 
            smpool->data.last + size <= smpool->data.end) {

            smpool->data.last += size;
            a->nalloc += n;

        } else {
            sb_size_t nalloc = 2 * ((n > a->nalloc) ? n : a->nalloc);
            sb_void_t *chunk = sb_spool_alloc(smpool, nalloc);
            if (!chunk) {
                return nullptr;
            }

            sb_memcpy(chunk, a->elts, a->nelts * a->size);
            a->elts = chunk;
            a->nalloc = nalloc;
        }
    }

    elt = (sb_bytep_t)a->elts + a->size * a->nelts;
    a->nelts += n;

    return elt;
}


sb_void_t *sb_array_pop(sb_array_t *a)
{
    if (a->nelts == 0) {
        return nullptr;
    }

    a->nelts--;
    return (sb_bytep_t)a->elts + a->size * a->nelts;
}


sb_void_t *sb_array_pop_n(sb_array_t *a, sb_size_t n)
{
    if (a->nelts == 0) {
        return nullptr;

    } else if (a->nelts - n < (sb_size_t)0) {
        a->nelts = 0;

    } else {
        a->nelts -= n;
    }

    return (sb_bytep_t)a->elts + a->size * a->nelts;
}


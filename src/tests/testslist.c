#include <sb/container/sb_slist.h>
#include <stdio.h>

typedef struct {
    sb_int_t data;
    sb_slist_node_t node;
}sb_slist_test_t;

sb_bool_t slist_magic(sb_slist_node_t *node)
{
    if (sb_slist_element(node, sb_slist_test_t, node)->data == 50) {
        return false;
    }
    sb_slist_element(node, sb_slist_test_t, node)->data *= 3;
    return true;
}

sb_void_t test_slist()
{
    sb_slist_t slist;
    sb_slist_test_t test_array[100];
    sb_slist_node_t *iterator;
    sb_slist_test_t *slist_data;
    sb_int_t i;

    sb_slist_make(&slist);

    for (i = 0; i < 100; i++) {
        test_array[i].data = i + 1;
        sb_slist_push_back(&slist, &test_array[i].node);
    }

    sb_slist_remove(&slist, &test_array[50].node);

    sb_slist_for_each(&slist, iterator) {
        slist_data = sb_slist_element(iterator, sb_slist_test_t, node);
        printf("%d\t", slist_data->data);
    }

    printf("\n");

    sb_slist_reverse(&slist);

    sb_slist_for_each(&slist, iterator) {
        slist_data = sb_slist_element(iterator, sb_slist_test_t, node);
        printf("%d\t", slist_data->data);
    }

    printf("\n");

    sb_slist_clear(&slist, slist_magic);
    sb_slist_for_each(&slist, iterator) {
        slist_data = sb_slist_element(iterator, sb_slist_test_t, node);
        printf("%d\t", slist_data->data);
    }
}

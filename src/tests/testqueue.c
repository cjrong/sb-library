

#include <sb/container/sb_queue.h>
#include <stdio.h>

typedef struct {
  sb_int_t data;
  sb_queue_node_t node;
}sb_queue_test_t;

void testqueue(void)
{
    sb_queue_test_t  queue_test_array[10];
    sb_queue_t       queue;
    sb_int_t         i;

    sb_queue_make(&queue);

    for (i = 0; i < 10; i++) {
        queue_test_array[i].data = i;
        sb_queue_enter(&queue, &queue_test_array[i].node);
    }

    while (!sb_queue_is_empty(&queue)) {
        sb_queue_node_t *iterator = sb_queue_leave(&queue);
        sb_queue_test_t *queue_data = sb_queue_element(iterator, sb_queue_test_t, node);
        printf("%d\t", queue_data->data);
    }
}


#include <sb/sb_container.h>
#include <stdio.h>
#include <math.h>
#include <time.h>
#include <stdlib.h>
#include <string.h>
/*
typedef struct {
    sb_int_t          key;
    sb_avltree_node_t node;
}sb_avltest_t;

static sb_int_t compare(const sb_avltree_node_t *node1, const sb_avltree_node_t *node2)
{
    sb_avltest_t *p1 = sb_avltree_data(node1, sb_avltest_t, node);
    sb_avltest_t *p2 = sb_avltree_data(node2, sb_avltest_t, node);
    return p1->key - p2->key;
}
 
static sb_void_t sb_avltree_insert_example(sb_avltree_t *tree, sb_avltest_t *node)
{
    sb_avltree_node_t **entry = &tree->root, *parent = nullptr;
 
    while (*entry) {
        sb_int_t compare;
        parent = *entry;
 
        compare = node->key - sb_avltree_data(parent, sb_avltest_t, node)->key;
        if (compare == 0)
            return;
        entry = compare < 0 ? &parent->left : &parent->right;
    }
     
    sb_avltree_link_node(entry, parent, &node->node);
    sb_avltree_insert_fixup(tree, parent);
}

static sb_void_t sb_avltree_remove_example(sb_avltree_t *tree, sb_int_t key) 
{
    sb_avltree_node_t *erase = tree->root;
    sb_avltest_t *current;

    while (erase) {
        current = sb_avltree_data(erase, sb_avltest_t, node);

        if (current->key == key) {
            break;
        }

        erase = current->key > key ? erase->left : erase->right;
    }

    if (!erase) {
        return ;
    }
    
    sb_avltree_remove_fixup(tree, erase);
}

sb_avltest_t rb_test_array[50000000];

int main(void)
{    
    sb_avltree_t tree;
    sb_avltree_node_t *iterator;
    sb_avltest_t find;
    clock_t start;
    int i;
    int arr[] = {3,2,1,4,5,6,7,16,15,14,13,12,11,10,8,9};

    sb_avltree(&tree, compare);

    srand((sb_uint_t)time(nullptr));

    start = clock();
    for (i = 0; i < sizeof(arr)/sizeof(int); i++) {
        rb_test_array[i].key = arr[i];
        sb_avltree_insert(&tree, &rb_test_array[i].node);
    }

    for (iterator = sb_avltree_begin(&tree);
         iterator != sb_avltree_end(&tree);
         iterator = sb_avltree_next(iterator)) {
        printf("%d\t", sb_avltree_data(iterator, sb_avltest_t, node)->key);
    }


    printf("\n%ld\n", clock() - start);
    return 0;
}

*/

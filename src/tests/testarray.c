

#include <sb/container/sb_array.h>
#include <sb/memory/sb_spool.h>
#include <stdio.h>


int aaaaa(void)
{
    sb_size_t i;
    sb_int_t *p;
    sb_array_t *arr;
    sb_spool_t *pool;

    pool = sb_spool_create(SB_ALIGNMENT, 102458);

    arr = sb_array_create(pool, sizeof(int), 5);

    for (i = 0; i <= 10240; i++) {
        p = sb_array_push(arr);
        *p = i;
    }

    sb_array_for_each(arr, p, i) {
        printf("%d\t", p[i]);
    }

    p = sb_array_pop(arr);
    printf("%d", *p);
   
    sb_spool_destory(pool);
    return 0;
}



#include <sb/container/sb_splaytree.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

typedef struct {
    sb_int_t            key;
    sb_splaytree_node_t node;
}sb_splaytree_test_t;

sb_splaytree_test_t* sb_splaytree_search_example(sb_splaytree_t *tree, sb_int_t key)
{
    sb_splaytree_node_t *current = tree->root;

    while (current) {
        sb_int_t compare_value = sb_splaytree_element(current, sb_splaytree_test_t, node)->key - key;
        if (compare_value == 0) {
            break;
        }

        current = compare_value > 0 ? current->left : current->right;
    }

    if (!current) {
        return nullptr;
    }

    sb_splaytree_splay(tree, current);

    return sb_splaytree_element(current, sb_splaytree_test_t, node);
}

sb_void_t sb_splaytree_remove_example(sb_splaytree_t *tree, sb_int_t key)
{
    sb_splaytree_node_t *current = tree->root;

    while (current) {
        sb_int_t compare_value = sb_splaytree_element(current, sb_splaytree_test_t, node)->key - key;
        if (compare_value == 0) {
            break;
        }

        current = compare_value > 0 ? current->left : current->right;
    }

    if (!current) {
        return;
    }

    sb_splaytree_remove(tree, current);
}

sb_void_t sb_splaytree_insert_example(sb_splaytree_t *tree, sb_splaytree_test_t *node)
{
    sb_splaytree_node_t **current = &tree->root, *parent = nullptr;

    while (*current) {
        sb_int_t compare;
        parent = *current;

        compare = node->key - sb_splaytree_element(parent, sb_splaytree_test_t, node)->key;
        if (compare == 0) {
            return;
        }

        current = compare < 0 ? &parent->left : &parent->right;
    }

    sb_splaytree_link_node(current, parent, &node->node);
    sb_splaytree_splay(tree, &node->node);
}

sb_int_t test_compare(const sb_splaytree_node_t *node1, const sb_splaytree_node_t *node2)
{
    sb_splaytree_test_t *p1 = sb_splaytree_element(node1, sb_splaytree_test_t, node);
    sb_splaytree_test_t *p2 = sb_splaytree_element(node2, sb_splaytree_test_t, node);
    return p1->key - p2->key;
}

sb_bool_t test_magic(sb_splaytree_node_t *node)
{
    sb_splaytree_test_t *data = sb_splaytree_element(node, sb_splaytree_test_t, node);
    data->key *= 2;
    return true;
}

sb_bool_t test_clear(sb_splaytree_node_t *node)
{
    sb_splaytree_test_t *data = sb_splaytree_element(node, sb_splaytree_test_t, node);
    if (data->key == 50) {
        return false;
    }
    return true;
}

void test_splaytree(void)
{
    sb_int_t i;
    sb_splaytree_t tree;
    sb_splaytree_test_t *data;
    sb_splaytree_test_t search;
    sb_splaytree_node_t *iterator;
    sb_splaytree_node_t *next_iterator;
    sb_splaytree_test_t splay_test_arr[100];

    sb_splaytree_make(&tree);
    srand((sb_uint_t)time(nullptr));

    {
        printf("test insert_if search_if remove_if:\n");
        for (i = 0; i < 100; i++) {
            splay_test_arr[i].node.key = splay_test_arr[i].key = i;
            sb_splaytree_insert_if(&tree, &splay_test_arr[i].node, test_compare);
        }

        sb_splaytree_for_each(&tree, iterator) {
            data = sb_splaytree_element(iterator, sb_splaytree_test_t, node);
            printf("%d\t", data->key);
        }

        sb_splaytree_remove_if(&tree, &splay_test_arr[50].node, test_compare);

        printf("\nremove 50:\n");
        sb_splaytree_for_each(&tree, iterator) {
            data = sb_splaytree_element(iterator, sb_splaytree_test_t, node);
            printf("%d\t", data->key);
        }

        search.key = 20;
        iterator = sb_splaytree_search_if(&tree, &search.node, test_compare);
        printf("search 20:%d\n", sb_splaytree_element(iterator, sb_splaytree_test_t, node)->key);
    }

    {
        printf("test for_each\n");

        sb_splaytree_clear(&tree, nullptr);

        for (i = 0; i < 100; i++) {
            splay_test_arr[i].node.key = splay_test_arr[i].key = i;
            sb_splaytree_insert_if(&tree, &splay_test_arr[i].node, test_compare);
        }

        printf("for each:\n");
        sb_splaytree_for_each(&tree, iterator) {
            data = sb_splaytree_element(iterator, sb_splaytree_test_t, node);
            printf("%d\t", data->key);
        }

        printf("\nreverse for each:\n");
        sb_splaytree_reverse_for_each(&tree, iterator) {
            data = sb_splaytree_element(iterator, sb_splaytree_test_t, node);
            printf("%d\t", data->key);
        }

        printf("\nsafe for each:\n");
        sb_splaytree_safe_for_each(&tree, iterator, next_iterator) {
            data = sb_splaytree_element(iterator, sb_splaytree_test_t, node);
            if (data->key == 50) {
                iterator = sb_splaytree_remove_if(&tree, iterator, test_compare);
                break;
            }
        }

        if (iterator != nullptr) {
            data = sb_splaytree_element(iterator, sb_splaytree_test_t, node);
            printf("remove: %d\n", data->key);
        }

        sb_splaytree_for_each(&tree, iterator) {
            data = sb_splaytree_element(iterator, sb_splaytree_test_t, node);
            printf("%d\t", data->key);
        }

        printf("\nsafe reverse for each:\n");
        sb_splaytree_safe_reverse_for_each(&tree, iterator, next_iterator) {
            data = sb_splaytree_element(iterator, sb_splaytree_test_t, node);
            if (data->key == 20) {
                iterator = sb_splaytree_remove_if(&tree, iterator, test_compare);
                break;
            }
        }

        if (iterator != nullptr) {
            data = sb_splaytree_element(iterator, sb_splaytree_test_t, node);
            printf("remove: %d\n", data->key);
        }

        sb_splaytree_for_each(&tree, iterator) {
            data = sb_splaytree_element(iterator, sb_splaytree_test_t, node);
            printf("%d\t", data->key);
        }
    }

    {
        printf("min max:\n");
        sb_splaytree_clear(&tree, nullptr);

        for (i = 0; i < 100; i++) {
            splay_test_arr[i].node.key = splay_test_arr[i].key = rand();
            sb_splaytree_insert_if(&tree, &splay_test_arr[i].node, test_compare);
        }

        sb_splaytree_for_each(&tree, iterator) {
            data = sb_splaytree_element(iterator, sb_splaytree_test_t, node);
            printf("%d\t", data->key);
        }

        printf("\nmin:%d\n", sb_splaytree_element(sb_splaytree_min(&tree), sb_splaytree_test_t, node)->key);
        printf("max:%d\n", sb_splaytree_element(sb_splaytree_max(&tree), sb_splaytree_test_t, node)->key);
    }

    {
        printf("test clear magic:\n");
        sb_splaytree_clear(&tree, nullptr);

        for (i = 0; i < 100; i++) {
            splay_test_arr[i].key = i;
            sb_splaytree_insert_if(&tree, &splay_test_arr[i].node, test_compare);
        }

        sb_splaytree_clear(&tree, test_clear);

        printf("test clear:\n");
        sb_splaytree_for_each(&tree, iterator) {
            data = sb_splaytree_element(iterator, sb_splaytree_test_t, node);
            printf("%d\t", data->key);
        }

        sb_splaytree_clear(&tree, nullptr);

        for (i = 0; i < 100; i++) {
            splay_test_arr[i].key = i;
            sb_splaytree_insert_if(&tree, &splay_test_arr[i].node, test_compare);
        }

        sb_splaytree_magic(&tree, test_magic);

        printf("\ntest magic:\n");
        sb_splaytree_for_each(&tree, iterator) {
            data = sb_splaytree_element(iterator, sb_splaytree_test_t, node);
            printf("%d\t", data->key);
        }
    }

    {
        sb_splaytree_clear(&tree, nullptr);

        for (i = 0; i < 100; i++) {
            splay_test_arr[i].node.key = splay_test_arr[i].key = i;
            sb_splaytree_insert_example(&tree, &splay_test_arr[i]);
        }
        printf("test insert example:\n");

        sb_splaytree_for_each(&tree, iterator) {
            data = sb_splaytree_element(iterator, sb_splaytree_test_t, node);
            printf("%d\t", data->key);
        }

        sb_splaytree_remove_example(&tree, 90);

        sb_splaytree_for_each(&tree, iterator) {
            data = sb_splaytree_element(iterator, sb_splaytree_test_t, node);
            printf("%d\t", data->key);
        }

        data = sb_splaytree_search_example(&tree, 40);
        printf("\nsearch : %d\n", data->key);
    }
}

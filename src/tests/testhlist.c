

#include <sb/container/sb_hash.h>
#include <stdio.h>

typedef struct {
    sb_int_t data;
    sb_hlist_node_t node;
}sb_hlist_test_t;

typedef struct {
    sb_int_t data;
    sb_hslist_node_t node;
}sb_hslist_test_t;

void test_hslist(void)
{
    sb_int_t i;
    sb_hslist_t hslist;
    sb_hslist_t hslist2;
    sb_hslist_node_t *iterator;
    sb_hslist_node_t *next_iterator;
    sb_hslist_test_t *data;
    sb_hslist_test_t test_hslist_arr[100];

    sb_hslist_make(&hslist);
    sb_hslist_make(&hslist2);

    for (i = 0; i < 100; i++) {
        test_hslist_arr[i].data = i;
        sb_hslist_insert(&hslist, &test_hslist_arr[i].node);
    }

    printf("\n\n");
    sb_hslist_for_each(&hslist, iterator) {
        data = sb_hslist_element(iterator, sb_hslist_test_t, node);
        printf("%d\t", data->data);
    }

    sb_hslist_remove(&hslist, &test_hslist_arr[20].node);
    printf("\n\n");
    sb_hslist_for_each(&hslist, iterator) {
        data = sb_hslist_element(iterator, sb_hslist_test_t, node);
        printf("%d\t", data->data);
    }

    sb_hslist_move(&hslist, &hslist2);
    printf("\n\n");
    sb_hslist_for_each(&hslist2, iterator) {
        data = sb_hslist_element(iterator, sb_hslist_test_t, node);
        printf("%d\t", data->data);
    }

    sb_hslist_safe_for_each(&hslist2, iterator, next_iterator) {
        data = sb_hslist_element(iterator, sb_hslist_test_t, node);
        if (data->data == 50) {
            sb_hslist_remove(&hslist2, iterator);
        }
    }

    printf("\n\n");
    sb_hslist_for_each(&hslist2, iterator) {
        data = sb_hslist_element(iterator, sb_hslist_test_t, node);
        printf("%d\t", data->data);
    }
}

void test_hlist(void)
{
    sb_int_t i;
    sb_hlist_t hlist;
    sb_hlist_t hlist2;
    sb_hlist_node_t *iterator;
    sb_hlist_node_t *next_iterator;
    sb_hlist_test_t *data;
    sb_hlist_test_t hlist_test_arr[100];

    sb_hlist_make(&hlist);
    sb_hlist_make(&hlist);

    for (i = 0; i < 100; i++) {
        hlist_test_arr[i].data = i;
        sb_hlist_insert(&hlist, &hlist_test_arr[i].node);
    }

    sb_hlist_for_each(&hlist, iterator) {
        data = sb_hlist_element(iterator, sb_hlist_test_t, node);
        printf("%d\t", data->data);
    }

    sb_hlist_remove(&hlist_test_arr[50].node);
    printf("\n\n");
    sb_hlist_for_each(&hlist, iterator) {
        data = sb_hlist_element(iterator, sb_hlist_test_t, node);
        printf("%d\t", data->data);
    }

    sb_hlist_insert_before(&hlist_test_arr[99].node, &hlist_test_arr[50].node);
    printf("\n\n");
    sb_hlist_for_each(&hlist, iterator) {
        data = sb_hlist_element(iterator, sb_hlist_test_t, node);
        printf("%d\t", data->data);
    }

    sb_hlist_remove(&hlist_test_arr[50].node);
    printf("\n\n");
    sb_hlist_for_each(&hlist, iterator) {
        data = sb_hlist_element(iterator, sb_hlist_test_t, node);
        printf("%d\t", data->data);
    }

    sb_hlist_insert_after(&hlist_test_arr[99].node, &hlist_test_arr[50].node);
    printf("\n\n");
    sb_hlist_for_each(&hlist, iterator) {
        data = sb_hlist_element(iterator, sb_hlist_test_t, node);
        printf("%d\t", data->data);
    }


    sb_hlist_safe_for_each(&hlist, iterator, next_iterator) {
        data = sb_hlist_element(iterator, sb_hlist_test_t, node);
        if (data->data == 20) {
            sb_hlist_remove(iterator);
        }
    }

    printf("\n\n");
    sb_hlist_for_each(&hlist, iterator) {
        data = sb_hlist_element(iterator, sb_hlist_test_t, node);
        printf("%d\t", data->data);
    }

    sb_hlist_move(&hlist, &hlist2);

    printf("\n\n");
    sb_hlist_for_each(&hlist2, iterator) {
        data = sb_hlist_element(iterator, sb_hlist_test_t, node);
        printf("%d\t", data->data);
    }
}



#include <sb/memory/sb_pool.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <time.h>

int main(void)
{

    sb_pool_t *pool;
    sb_int_t   i;
    sb_int_t  *ptr;
    clock_t start = clock();
    pool = sb_pool_default();

    srand((sb_ulong_t)time(nullptr));

    ptr = sb_pool_alloc(pool, 1024);

    
    ptr = sb_pool_alloc(pool, 0);
    for (i = 0; i < 10000000; i++) {
       
        ptr = sb_pool_alloc(pool, sizeof(int));
        sb_pool_free(pool, ptr, sizeof(int));
    }
    

   


    printf("%d\n", clock() - start);
    sb_pool_destroy(pool);

    return 0;
}

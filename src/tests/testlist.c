#include <sb/sb_container.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

typedef struct {
    int             data;
    sb_list_node_t  node;
}sb_list_test_t;

sb_int_t list_compare(const sb_list_node_t *node1, const sb_list_node_t *node2)
{
    sb_list_test_t *p1 = sb_list_element(node1, sb_list_test_t, node);
    sb_list_test_t *p2 = sb_list_element(node2, sb_list_test_t, node);
    return p1->data - p2->data;
}

sb_bool_t list_clear_magic(sb_list_node_t *node)
{
    sb_list_test_t *p = sb_list_element(node, sb_list_test_t, node);
    p->data = p->data * 2;
    return true;
}

sb_void_t test_list(sb_void_t)
{
    sb_int_t        i;
    sb_list_t       list;
    sb_list_test_t *list_data;
    sb_list_node_t *iterator;
    sb_list_test_t  test_list[100];

    srand((sb_uint_t)time(nullptr));

    {
        printf("test list insert:\n");
        sb_list_make(&list);

        for (i = 0; i < 100; i++) {
            test_list[i].data = i;
            sb_list_add(&list, &test_list[i].node);
        }

        printf("for each:\n");
        sb_list_for_each(&list, iterator) {
            list_data = sb_list_element(iterator, sb_list_test_t, node);
            if (list_data) {
                printf("%d\t", list_data->data);
            }
        }

        printf("reversal for each:\n");
        sb_list_reverse_for_each(&list, iterator) {
            list_data = sb_list_element(iterator, sb_list_test_t, node);
            if (list_data) {
                printf("%d\t", list_data->data);
            }
        }
    }

    {
        printf("test list insert tail:\n");
        sb_list_clear(&list, nullptr);

        for (i = 0; i < 100; i++) {
            test_list[i].data = i * 2;
            sb_list_add_tail(&list, &test_list[i].node);
        }

        sb_list_for_each(&list, iterator) {
            list_data = sb_list_element(iterator, sb_list_test_t, node);
            printf("%d\t", list_data->data);
        }
    }

    {
        printf("test iterator:\n");

        sb_list_clear(&list, nullptr);

        for (i = 0; i < 100; i++) {
            test_list[i].data = i + 1;
            sb_list_push_back(&list, &test_list[i].node);
        }

        for (iterator = sb_list_begin(&list);
             iterator != sb_list_end(&list);
             iterator = sb_list_next(iterator)) {
            list_data = sb_list_element(iterator, sb_list_test_t, node);
            printf("%d\t", list_data->data);
        }

        printf("\n\n");
        for (iterator = sb_list_rbegin(&list);
             iterator != sb_list_rend(&list);
             iterator = sb_list_rnext(iterator)) {
            list_data = sb_list_element(iterator, sb_list_test_t, node);
            printf("%d\t", list_data->data);
        }

        printf("\n\n");
        for (iterator = sb_list_rbegin(&list);
             iterator != sb_list_rend(&list);
             iterator = sb_list_prev(iterator)) {
            list_data = sb_list_element(iterator, sb_list_test_t, node);
            printf("%d\t", list_data->data);
        }

        printf("\n\n");
        for (iterator = sb_list_begin(&list);
             iterator != sb_list_end(&list);
             iterator = sb_list_rprev(iterator)) {
            list_data = sb_list_element(iterator, sb_list_test_t, node);
            printf("%d\t", list_data->data);
        }
    }

    {
        printf("test safe remove:\n");
    }

    {
        printf("test list reversal:\n");
        sb_list_clear(&list, nullptr);

        for (i = 0; i < 100; i++) {
            test_list[i].data = i;
            sb_list_push_back(&list, &test_list[i].node);
        }

        printf("reversal before:\n");
        sb_list_for_each(&list, iterator) {
            list_data = sb_list_element(iterator, sb_list_test_t, node);
            printf("%d\t", list_data->data);
        }

        sb_list_reverse(&list);

        printf("reversal:\n");

        sb_list_for_each(&list, iterator){
            list_data = sb_list_element(iterator, sb_list_test_t, node);
            printf("%d\t", list_data->data);
        }
    }

    {
        sb_list_t list1;
        sb_list_t list2;

        printf("test list splice:\n");
        sb_list_make(&list1);
        sb_list_make(&list2);
        sb_list_clear(&list, nullptr);

        for (i = 0; i < 50; i++) {
            test_list[i].data = i;
            sb_list_push_back(&list1, &test_list[i].node);
        }

        for (i = 50; i < 100; i++) {
            test_list[i].data = i;
            sb_list_push_back(&list2, &test_list[i].node);
        }

        sb_list_splice(&list, &list1);

        printf("splice list1:\n");
        sb_list_for_each(&list, iterator) {
            list_data = sb_list_element(iterator, sb_list_test_t, node);
            printf("%d\t", list_data->data);
        }

        printf("splice list2:\n");
        sb_list_splice_tail(&list, &list2);

        sb_list_for_each(&list, iterator) {
            list_data = sb_list_element(iterator, sb_list_test_t, node);
            printf("%d\t", list_data->data);
        }   
    }

   
    {
        sb_list_t list1;
        sb_list_t list2;
        sb_list_node_t *slice_pos = nullptr;

        printf("\n\ntest list slice:\n");

        sb_list_make(&list1);
        sb_list_make(&list2);

        for (i = 0; i < 100; i++) {
            test_list[i].data = i;
            sb_list_push_back(&list1, &test_list[i].node);
            if (i == 50) {
                slice_pos = &test_list[i].node;
            }
        }

        sb_list_slice(&list2, &list1, slice_pos);

        printf("\nlist1:\n");
        sb_list_for_each(&list1, iterator) {
            list_data = sb_list_element(iterator, sb_list_test_t, node);
            printf("%d\t", list_data->data);
        }

        printf("\nlist2:\n");
        sb_list_for_each(&list2, iterator) {
            list_data = sb_list_element(iterator, sb_list_test_t, node);
            printf("%d\t", list_data->data);
        }
    }

    {
        sb_list_clear(&list, nullptr);

        printf("\ntest list sort\n");

        for (i = 0; i < 100; i++) {
            test_list[i].data = rand() % 10000;
            sb_list_push_back(&list, &test_list[i].node);
        }

        printf("\nsort before:\n");

        sb_list_for_each(&list, iterator) {
            list_data = sb_list_element(iterator, sb_list_test_t, node);
            if (list_data) {
                printf("%d\t", list_data->data);
            }
        }

        sb_list_insertion_sort(&list, list_compare);

        printf("\nsort result:\n");

        sb_list_for_each(&list, iterator) {
            list_data = sb_list_element(iterator, sb_list_test_t, node);
            if (list_data) {
                printf("%d\t", list_data->data);
            }
        }

        printf("\n");
    }
}



#include <sb/sb_container.h>
#include <stdio.h>
#include <math.h>
#include <time.h>
#include <stdlib.h>
#include <string.h>

typedef struct {
    sb_int_t          key;
    sb_rbtree_node_t node;
}sb_rb_test_t;
sb_rb_test_t rb_test_array[10000000];

static sb_int_t test_compare(const sb_rbtree_node_t *node1, const sb_rbtree_node_t *node2)
{
    sb_rb_test_t *p1 = sb_rbtree_element(node1, sb_rb_test_t, node);
    sb_rb_test_t *p2 = sb_rbtree_element(node2, sb_rb_test_t, node);
    return p1->key - p2->key;
}

static sb_void_t sb_rbtree_insert_example(sb_rbtree_t *tree, sb_rb_test_t *node)
{
    sb_rbtree_node_t **entry = &tree->root, *parent = nullptr;

    while (*entry) {
        sb_int_t compare;
        parent = *entry;

        compare = node->key - sb_rbtree_element(parent, sb_rb_test_t, node)->key;
        if (compare == 0)
            return;
        entry = compare < 0 ? &parent->left : &parent->right;
    }

    sb_rbtree_link_node(entry, parent, &node->node);
    sb_rbtree_insert(tree, &node->node);
}

/*
int main(void)
{
    sb_rbtree_t tree;
    clock_t start;
    sb_rb_test_t find;
    sb_rb_test_t *rb_data;
    sb_rbtree_node_t *iterator, *next_iterator;

    int i;
    sb_rbtree(&tree);
    srand((sb_uint_t)time(nullptr));

    start = clock();
    for (i = 0; i < 100; i++) {
        rb_test_array[i].key = i;
        sb_rbtree_insert_example(&tree, &rb_test_array[i]);
        sb_rbtree_insert_if(&tree, &rb_test_array[i].node, test_compare);
    }
    sb_rbtree_safe_for_each(&tree, iterator, next_iterator) {
        rb_data = sb_rbtree_element(iterator, sb_rb_test_t, node);
        if (rb_data->key == 50) {
            sb_rbtree_remove(&tree, iterator);
        }
    }
    sb_rbtree_reverse_for_each(&tree, iterator) {
        rb_data = sb_rbtree_element(iterator, sb_rb_test_t, node);
        printf("%d\t", rb_data->key);
    }
    printf("\n");
    sb_rbtree_safe_reverse_for_each(&tree, iterator, next_iterator) {
        rb_data = sb_rbtree_element(iterator, sb_rb_test_t, node);
        printf("%d\t", rb_data->key);
    }
    printf("%ld\n", clock() - start);
    return 0;
}
*/